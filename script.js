class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }

  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
  }
  
  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary) {
    super(name, age, salary);
    this.lang = ["JS", "Java", "C++"];
  }
  get salary() {
    return this._salary * 3;
  }
}

const programmer1 = new Programmer("Volodymyr", 39, 3000);
const programmer2 = new Programmer("Alex", 29, 2000);
const programmer3 = new Programmer("Max", 34, 5000);

console.log({ programmer1, programmer2, programmer3 });
console.log({
  "Salary of programmer1": programmer1.salary,
  "Salary of programmer2": programmer2.salary,
  "Salary of programmer3": programmer3.salary,
});
