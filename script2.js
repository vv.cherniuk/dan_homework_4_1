function Employee(name, age, salary) {
  this.name = name;
  this.age = age;
  this.salary = salary;
}

Employee.prototype.getSalary = function () {
  return this.salary;
};

Employee.prototype.getName = function () {
  return this.name;
};

Employee.prototype.getAge = function () {
  return this.age;
};

Employee.prototype.setSalary = function (salary) {
  this.salary = salary;
};

Employee.prototype.setName = function (name) {
  this.name = name;
};

Employee.prototype.setAge = function (age) {
  this.age = age;
};

function Programmer(name, age, salary) {
  Employee.call(this, name, age, salary);
}

Programmer.prototype = Object.create(Employee.prototype);

Programmer.prototype.getSalary = function () {
  return this.salary * 3;
};

const programmer1 = new Programmer("Volodymyr", 30, 3000);
const programmer2 = new Programmer("Max", 29, 4000);
const programmer3 = new Programmer("Andrey", 30, 5000);

const div = document.getElementById("div");

const salaries = {
  "Programmer 1 salary": programmer1.getSalary(),
  "Programmer 2 salary": programmer2.getSalary(),
  "Programmer 3 salary": programmer3.getSalary(),
};
for (let key in salaries) {
  div.innerHTML += `<p>${key}: ${salaries[key]}</p>`;
}

console.log(programmer1, programmer2, programmer3);
console.log({
  "Programmer 1 salary": programmer1.getSalary(),
  "Programmer 2 salary": programmer2.getSalary(),
  "Programmer 3 salary": programmer3.getSalary(),
});
